angular.element(document).ready(function() {
    var cf_miniform_generator = angular.module('generator', ['angucomplete-alt', 'obDateRangePicker', 'toastr', 'angular-clipboard'])
    .config(['$httpProvider', function($httpProvider) {
	    delete $httpProvider.defaults.headers.common['X-Requested-With'];
	}])
    .config(function(toastrConfig) {
        angular.extend(toastrConfig, {
            allowHtml: true,
            autoDismiss: true,
            maxOpened: 1,
            timeOut: 3500,
            preventOpenDuplicates: true,
        });
    })
    .controller('generatorCtrl', function($scope, toastr, $http){

        //Strings
        $scope.autocmplt_placeholder = "π.χ. Athens";

        //Variables
        $scope.hostIp = "http://128.199.46.240:8087/?s=";
        $scope.directiveIsVisible=false;
        $scope.language = "el";
        $scope.directiveCode = "";
        $scope.destinationip = "";
        $scope.destination_ctrl = {};
        $scope.whatToSearch = "destination";
        $scope.search_term = "προορισμό"
        $scope.auto_field_req="true";
        $scope.input_field_req="true";

        //Functions
        $scope.fetchHotel = function (){
            $http.get('http://178.62.220.40:8091/api/hotels/get/' + $scope.hotelurl)
            .then(function(response){
                $scope.data=response.data[0];
                $scope.showHotelInfo = true;
            });
        };

        $scope.termChanged = function() {
            if($scope.whatToSearch=='destination'){
                $scope.search_term="προορισμό";
                $scope.autocmplt_placeholder = "π.χ. Athens";
            }else if($scope.whatToSearch=='hotel'){
                $scope.search_term="id ξενοδοχείου";
                $scope.autocmplt_placeholder = "π.χ.  http://www.hotel.com/hotel/";
            }
        }

        $scope.langChange = function(lang){
            $scope.language = lang;
        };

        $scope.renderForm = function() {
            if($scope.destinationip!=""){
                if($scope.whatToSearch=='destination'){
                    $scope.simpleDirectiveIsVisible=true;
                    $scope.hotelDirectiveIsVisible=false;
                }else if($scope.whatToSearch=='hotel'){
                    $scope.hotelDirectiveIsVisible=true;
                    $scope.simpleDirectiveIsVisible=false;
                }
            }else{
                $scope.hotelDirectiveIsVisible=false;
                $scope.simpleDirectiveIsVisible=false;
            }
        }

        $scope.generateCode = function(){
            if($scope.whatToSearch=='destination'){
                if(typeof $scope.searchStr != 'undefined'){
                    if($scope.destinationip !=""){
                        $scope.directiveCode =  '<search-form-predefined' +
                                                ' otagid="' + $scope.searchStr.originalObject.otag_id +
                                                '" featureclass="'+ $scope.searchStr.originalObject.feature_class +
                                                '" namelt="'+ $scope.searchStr.originalObject.name_lt +
                                                '" latitude="' + $scope.searchStr.originalObject.latitude +
                                                '" longitude="' + $scope.searchStr.originalObject.longitude +
                                                '" dest-ip="' + $scope.destinationip +
                                                '" lang="' + $scope.language +
                                                '" ng-cloak'+
                                                '></search-form-predefined>';
                        $scope.directiveTemplate = '<div class="cf-searchform">\t<div class="cf-searchform-container">\n\t\t<form id="srch" name="search" class="booking-form" novalidate>\n\t\t\t<div class="cf-searchform-contents">\n\t\t\t\t<div class="cf-searchform-destination">\n\t\t\t\t\t<div class="cf-destination-container">\n\t\t\t\t\t\t<div class="cf-destination-label">\n\t\t\t\t\t\t\t<label class="cf-label">{{autocomplete_label}}</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="cf-destination">\n\t\t\t\t\t\t\t<h3>{{dest}}</h3>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="cf-searchform-dates">\n\t\t\t\t\t<div class="cf-dates-container">\n\t\t\t\t\t\t<div class="cf-dates-label">\n\t\t\t\t\t\t\t<label class="cf-label">{{datepicker_label}}</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="cf-datepicker">\n\t\t\t\t\t\t\t<ob-daterangepicker ng-class="{\'up\': opts.dropsUp}" class="{{opts.opens}}" min-day="opts.min" range = "opts.range" linked-calendars="opts.linked" on-apply="opts.rangeApplied(start, end)" format="opts.format" week-start="opts.weekStart" auto-apply="opts.autoApply" calendars-always-on="opts.calendarsAlwaysOn">\n\t\t\t\t\t\t\t</ob-daterangepicker>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="cf-searchform-persons">\n\t\t\t\t\t<div class="cf-persons-container">\n\t\t\t\t\t\t<div class="cf-persons-label">\n\t\t\t\t\t\t\t<label class="cf-label">{{adults_label}}</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="cf-person">\n\t\t\t\t\t\t\t<input type="number" min="1" max="10" ng-model="adults" class="cf-input">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="cf-searchform-children">\n\t\t\t\t\t<div class="cf-children-container">\n\t\t\t\t\t\t<div class="cf-children-label">\n\t\t\t\t\t\t\t<label class="search-form-label">{{children_label}}</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="cf-children">\n\t\t\t\t\t\t\t<input type="number" min="0" max="4" ng-model="children" class="cf-input">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="cf-searchform-children-ages" ng-if="children>0">\n\t\t\t\t\t<div class="cf-children-ages-container">\n\t\t\t\t\t\t<div class="cf-children-ages-label">\n\t\t\t\t\t\t\t<label class="search-form-label">{{ages_label}}</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="cf-children-ages">\n\t\t\t\t\t\t\t<div class="search-form-age-input-div" ng-repeat="child in numberOf(children)">\n\t\t\t\t\t\t\t\t<input type="number" min="0" max="17" ng-model="childrenAges[$index]" class="cf-input">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="cf-submit">\n\t\t\t\t\t<div class="cf-submit-container">\n\t\t\t\t\t\t<div class="cf-search-button btn btn-primary" ng-click="search()">{{search_btn_label}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</form>\n\t</div>\n</div>';
                        $scope.directiveScript='<script src="http://includes.onetourismo.com:8056/assets/js/onetourismo.min.js"></script>';
                        $scope.directiveStyle=  '<link rel="stylesheet" href="http://includes.onetourismo.com:8056/assets/css/style.css" />\n'+
                                                '<link rel="stylesheet" href="http://includes.onetourismo.com:8056/assets/css/angucomplete-alt.css">\n'+
                                                '<link rel="stylesheet" href="http://includes.onetourismo.com:8056/assets/css/bootstrap.css" />\n'+
                                                '<link rel="stylesheet" href="http://includes.onetourismo.com:8056/assets/css/ob-daterangepicker.css" />';
                    }else{
                        toastr.error('Δεν έχει οριστεί <a class="cf-underline" href="#step1">Σελίδα αποτελεσμάτων</a>','');
                        return ;
                    }
                }else{
                    toastr.error('Δεν έχει οριστεί <a class="cf-underline" href="#step1">Προορισμός</a>','');
                    return ;
                }
            }else if($scope.whatToSearch=='hotel'){
                if(typeof $scope.hotelurl != 'undefined'){
                    if($scope.destinationip !=""){
                        $scope.directiveCode =  '<search-form-hotel-predefined' +
                                                ' dest-ip="' + $scope.destinationip +
                                                '" hotelid="' + $scope.data._id +
                                                '" hotelname="'+ $scope.data.name +
                                                '" lang="' + $scope.language +
                                                '" ng-cloak'+
                                                '></search-form-hotel-predefined>';
                                        
                        $scope.directiveTemplate = '<div class="cf-searchform">\t<div class="cf-searchform-container">\n\t\t<form id="srch" name="search" class="booking-form" novalidate>\n\t\t\t<div class="cf-searchform-contents">\n\t\t\t\t<div class="cf-searchform-destination">\n\t\t\t\t\t<div class="cf-destination-container">\n\t\t\t\t\t\t<div class="cf-destination-label">\n\t\t\t\t\t\t\t<label class="cf-label">{{autocomplete_label}}</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="cf-destination">\n\t\t\t\t\t\t\t<h3>{{dest}}</h3>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="cf-searchform-dates">\n\t\t\t\t\t<div class="cf-dates-container">\n\t\t\t\t\t\t<div class="cf-dates-label">\n\t\t\t\t\t\t\t<label class="cf-label">{{datepicker_label}}</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="cf-datepicker">\n\t\t\t\t\t\t\t<ob-daterangepicker ng-class="{\'up\': opts.dropsUp}" class="{{opts.opens}}" min-day="opts.min" range = "opts.range" linked-calendars="opts.linked" on-apply="opts.rangeApplied(start, end)" format="opts.format" week-start="opts.weekStart" auto-apply="opts.autoApply" calendars-always-on="opts.calendarsAlwaysOn">\n\t\t\t\t\t\t\t</ob-daterangepicker>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="cf-searchform-persons">\n\t\t\t\t\t<div class="cf-persons-container">\n\t\t\t\t\t\t<div class="cf-persons-label">\n\t\t\t\t\t\t\t<label class="cf-label">{{adults_label}}</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="cf-person">\n\t\t\t\t\t\t\t<input type="number" min="1" max="10" ng-model="adults" class="cf-input">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="cf-searchform-children">\n\t\t\t\t\t<div class="cf-children-container">\n\t\t\t\t\t\t<div class="cf-children-label">\n\t\t\t\t\t\t\t<label class="search-form-label">{{children_label}}</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="cf-children">\n\t\t\t\t\t\t\t<input type="number" min="0" max="4" ng-model="children" class="cf-input">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="cf-searchform-children-ages" ng-if="children>0">\n\t\t\t\t\t<div class="cf-children-ages-container">\n\t\t\t\t\t\t<div class="cf-children-ages-label">\n\t\t\t\t\t\t\t<label class="search-form-label">{{ages_label}}</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="cf-children-ages">\n\t\t\t\t\t\t\t<div class="search-form-age-input-div" ng-repeat="child in numberOf(children)">\n\t\t\t\t\t\t\t\t<input type="number" min="0" max="17" ng-model="childrenAges[$index]" class="cf-input">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="cf-submit">\n\t\t\t\t\t<div class="cf-submit-container">\n\t\t\t\t\t\t<div class="cf-search-button btn btn-primary" ng-click="search()">{{search_btn_label}}</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</form>\n\t</div>\n</div>';
                        $scope.directiveScript='<script src="http://includes.onetourismo.com:8056/assets/js/onetourismo.min.js"></script>';
                        $scope.directiveStyle=  '<link rel="stylesheet" href="http://includes.onetourismo.com:8056/assets/css/style.css" />\n'+
                                                '<link rel="stylesheet" href="http://includes.onetourismo.com:8056/assets/css/angucomplete-alt.css">\n'+
                                                '<link rel="stylesheet" href="http://includes.onetourismo.com:8056/assets/css/bootstrap.css" />\n'+
                                                '<link rel="stylesheet" href="http://includes.onetourismo.com:8056/assets/css/ob-daterangepicker.css" />';
                    }else{
                        toastr.error('Δεν έχει οριστεί <a class="cf-underline" href="#step1">Σελίδα αποτελεσμάτων</a>','');
                        return ;
                    }
                }else{
                    toastr.error('Δεν έχει οριστεί <a class="cf-underline" href="#step1">Ξενοδοχείο</a>','');
                    return ;
                }
            }
        };

        $scope.copied = function(){
            toastr.success('Αντιγράφηκε','');
        }
    })
    .directive("searchFormHotelPredefined", function(toastr) {
        return {
            restrict: "E",
            templateUrl: 'template_hotels.html',
            scope: {
                destIp: "@",
                hotelid: "@",
                hotelname: "@",
                lang: "@"
            },

            link: function(scope) {

                //Variables
                scope.adults = 2;
                scope.children = 0;
                scope.childrenAges = [];
                var today = moment();
                var tomorrow = moment().add(1, 'days');
                scope.sdate = today.format('YYYY-MM-DD').toString();
                scope.edate = tomorrow.format('YYYY-MM-DD').toString();

                scope.$watch('lang', function () {
                    //languages
                    if(scope.lang=='el'){
                        scope.autocomplete_label = "Όνομα Ξενοδοχείου";
                        scope.autocmplt_placeholder = "πχ. Athens";
                        scope.datepicker_label = "Επιλέξτε ημερομηνίες";
                        scope.adults_label = "Ενήλικες";
                        scope.children_label = "Παιδιά";
                        scope.ages_label = "Ηλικίες Παιδιών";
                        scope.search_btn_label = "Αναζήτηση";
                    }else{
                        scope.autocomplete_label = "Hotel Name";
                        scope.autocmplt_placeholder = "ex. Athens";
                        scope.datepicker_label = "Choose dates";
                        scope.adults_label = "Adults";
                        scope.children_label = "Children";
                        scope.ages_label = "Children ages";
                        scope.search_btn_label = "Search";
                    }
                });

                scope.$watch('hotelname', function () {
                    scope.dest = scope.hotelname;
                });

                scope.search = function() {
                    if(scope.destIp==""||scope.hotelname==""){
                        toastr.error('Δεν έχει οριστεί <a class="cf-underline" href="#step1">Ξενοδοχείο</a> ή <a class="cf-underline" href="#step1">Σελίδα εμφάνισης αποτελεσμάτων</a>','');
                        return ;
                    }
                    var location = "";
                    location =  scope.destIp +
                                scope.lang  + '/' +
                                'hotel/' + 
                                scope.hotelid + "/" + 
                                scope.sdate + "/" + 
                                scope.edate + "/" + 
                                scope.adults + "/" + 
                                scope.children;
                                   
                    for (var i = 0; i < scope.childrenAges.length; i++) {
                        location += "/" + scope.childrenAges[i];
                    }
                    window.open(location);
                }

                scope.numberOf = function(num) {
                    var ar = [];
                    for (var i = 0; i < num; i++)
                        ar.push(i);
                    return ar;
                };            

                scope.opts = {
                    range: {
                        start: moment(),
                        end: tomorrow
                    },
                    autoApply: true,
                    min: moment(),
                    linked: true,
                    calendarsAlwaysOn: false,
                    weekStart: 'mo',
                    format: 'YYYY-MM-DD',
                    opens: 'down right'
                };
                scope.opts.rangeApplied = function(start, end){
                    scope.sdate = start.format('YYYY-MM-DD').toString();
                    scope.edate = end.format('YYYY-MM-DD').toString();
                }              
            }
        };
    })
    .directive("searchFormPredefined", function(toastr) {
	    return {
	        restrict: "E",
	        templateUrl: 'template.html',
	        scope: {
	            otagid: "@",
                featureclass: "@",
                namelt: "@",
                latitude: "@",
                longitude: "@",
	            destIp: "@",
	            lang: "@"
	        },

	        link: function(scope) {

                //Variables
	            scope.adults = 2;
	            scope.children = 0;
	            scope.childrenAges = [];
	            var today = moment();
	            var tomorrow = moment().add(1, 'days');
	            scope.sdate = today.format('YYYY-MM-DD').toString();
	            scope.edate = tomorrow.format('YYYY-MM-DD').toString();

                scope.$watch('lang', function () {
                    //languages
                    if(scope.lang=='el'){
                        scope.autocomplete_label = "Προορισμός";
                        scope.autocmplt_placeholder = "πχ. Athens";
                        scope.datepicker_label = "Επιλέξτε ημερομηνίες";
                        scope.adults_label = "Ενήλικες";
                        scope.children_label = "Παιδιά";
                        scope.ages_label = "Ηλικίες Παιδιών";
                        scope.search_btn_label = "Αναζήτηση";
                    }else{
                        scope.autocomplete_label = "Destination";
                        scope.autocmplt_placeholder = "ex. Athens";
                        scope.datepicker_label = "Choose dates";
                        scope.adults_label = "Adults";
                        scope.children_label = "Children";
                        scope.ages_label = "Children ages";
                        scope.search_btn_label = "Search";
                    }
                });

                scope.$watch('namelt', function () {
                    scope.dest = scope.namelt;
                });

	            scope.search = function() {
                    if(scope.destIp==""||scope.namelt==""){
                        toastr.error('Δεν έχει οριστεί <a class="cf-underline" href="#step1">Προορισμός</a> ή <a class="cf-underline" href="#step1">Σελίδα εμφάνισης αποτελεσμάτων</a>','');
                        return ;
                    }
	                var location = "";
	                location =  scope.destIp +
                                scope.lang  + '/' +'search/' + 
                                scope.sdate + "/" + 
                                scope.edate + "/" + 
                                scope.adults + "/" + 
                                scope.children + "/" + 
                                scope.otagid + "/" + 
                                scope.featureclass + "/" + 
                                scope.namelt + "/" + 
                                scope.latitude+ "/" + 
                                scope.longitude;
	                               
	                for (var i = 0; i < scope.childrenAges.length; i++) {
	                    location += "/" + scope.childrenAges[i];
	                }
	                window.open(location);
	            }

	            scope.numberOf = function(num) {
	                var ar = [];
	                for (var i = 0; i < num; i++)
	                    ar.push(i);
	                return ar;
	            };            

	            scope.opts = {
	                range: {
	                    start: moment(),
	                    end: tomorrow
	                },
	                autoApply: true,
	                min: moment(),
	                linked: true,
	                calendarsAlwaysOn: false,
	                weekStart: 'mo',
	                format: 'YYYY-MM-DD',
	                opens: 'down right'
	            };
	            scope.opts.rangeApplied = function(start, end){
	                scope.sdate = start.format('YYYY-MM-DD').toString();
	                scope.edate = end.format('YYYY-MM-DD').toString();
	            }              
	        }
	    };
	});angular.bootstrap(document, ['generator']);
});